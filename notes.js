// afficher du texte pour la développeuse
// console.log("coucou");
// console.error("error !");

// déclaration (initialise) de variable
let a;
// assignation d'une valeur
a = 2; // number reçoit 2
// code mort
// number = "tacos"; // langage permissif

// opérateurs ??? calcul, concaténation
// modulo %, / * + -
let b = 3;

let c = a * b;
console.log(c);

/*
      |
    7  |  2
______|_____
      |
    1  |  3
      |

1 -> reste de la division entière de 7 par 3 (c'est le modulo)
7 % 2 == 1

      |
    6  |  3
______|_____
      |
    0  |  2
      |

6 % 3 == 0

Un modulo ça sert à savoir si un nombre est pair ou impair
Si le nombre est pair : il reste forcément 0 lors de la division entière par 2
Si nombre % 2 est égal à 0

Si un nombre est divisible par 3 : nombre % 3 == 0
Si un nombre est divisible par 5 : nombre % 5 == 0
*/
const firstName = "Michel"; // constante : on ne peut plus changer la valeur
let lastName = "Durand"; // variable : on peut changer la valeur

lastName = "Brandon";